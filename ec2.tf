provider "aws"  {
  profile = "default"
  region = "ap-south-1"
}

resource "aws_instance" "myec2" {
  ami = "ami-025b4b7b37b743227"
  instance_type = "t2.micro"
  subnet_id = "subnet-0658b7d3a42ab862e"
  security_groups = ["sg-02a048f5d7d27c276"]
}
